package domain;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import domain.services.*;

public class Actor {
	
	private int id;
	private String name;
	private String surname;
	private List <FilmOfActor> films;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public List<FilmOfActor> getFilms() {
		return films;
	}
	public void setFilms(List<FilmOfActor> films) {
		this.films = films;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
}
