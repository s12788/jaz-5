package domain;

import java.util.List;

import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties(value = { "yaer", "rating" })
public class Film {
	
	private int id;
	private int year;
	private float rating;
	private String title;
	private String director;
	private String screenplay;
	private String genre;
	private String production;
	private String description;
	private List<Comment> comments;
	private List <Actor> cast;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getScreenplay() {
		return screenplay;
	}
	public void setScreenplay(String screenplay) {
		this.screenplay = screenplay;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getProduction() {
		return production;
	}
	public void setProduction(String production) {
		this.production = production;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List<Actor> getCast() {
		return cast;
	}
	public void setCast(List<Actor> cast) {
		this.cast = cast;
	}
	


	
	

}
